# Практическая работа по модулю "Ansible" курса "DevOps-инженер. Основы"

1. Устанавливаем [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

1. Устанавливаем [Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
   
1. Устанавливаем [Yandex Cloud CLI](https://cloud.yandex.ru/docs/cli/quickstart#install) и [настраиваем](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-quickstart)

1. Клонируем репозиторий
   ```shell
   git clone https://gitlab.com/skillbox_edu/devops/basic/module_9.git
   cd module_9
   ```
   
1. Инициализируем `terraform`
   ```shell
   terraform -chdir=./terraform init
   ```
   
1. Создаем ВМ на Yandex Cloud
   ```shell
   terraform -chdir=./terraform apply -auto-approve
   ```
   
   IP-адреса ВМ и балансировщика:
   
   ![ip-адреса ВМ и балансировщика](docs/imgs/task-1-ip-addresses.png)

1. Указываем ip-адреса nginx-серверов и react-приложения в файле [hosts](ansible/hosts) и в файле [nginx.yml](ansible/nginx.yml) в секции `vars`

1. Развертываем ReactJS-приложение
   ```shell
   cd ansible && ANSIBLE_CONFIG=ansible.cfg ansible-playbook -vv reactjs_playbook.yml; cd ..
   ```
   
   Запущенное ReactJS-приложение:

   ![Запущенное ReactJS-приложение](docs/imgs/task-1-react-app.png)
   
1. Развертываем nginx-сервера
   ```shell
   cd ansible && ANSIBLE_CONFIG=ansible.cfg ansible-playbook -vv nginx.yml; cd ..
   ```
   
   Доступ через балансировщик:

   ![Доступ через балансировщик](docs/imgs/task-1-nginx-load-balancer.png)

1. Удаляем ресурсы Yandex Cloud
   ```shell
   terraform -chdir=./terraform destroy -auto-approve
   ```

## Ansible-роли

1. Создаем ВМ на Yandex Cloud
   ```shell
   terraform -chdir=./terraform apply -auto-approve
   ```

1. Указываем ip-адреса nginx-серверов и react-приложения в файле [hosts](ansible/hosts)

1. Развертываем ReactJS-приложение и nginx-сервера
   ```shell
   cd ansible && ANSIBLE_CONFIG=ansible.cfg ansible-playbook -vv provision.yml; cd ..
   ```
   
   Запущенное ReactJS-приложение:

   ![Запущенное ReactJS-приложение](docs/imgs/task-2-react-app.png)

   Доступ через балансировщик:

   ![Доступ через балансировщик](docs/imgs/task-2-nginx-load-balancer.png)

1. Удаляем ресурсы Yandex Cloud
   ```shell
   terraform -chdir=./terraform destroy -auto-approve
   ```
