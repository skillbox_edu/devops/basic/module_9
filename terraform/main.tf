terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "= 0.82"
    }
  }
  required_version = "= 1.3.4"
}

provider "yandex" {
  zone = "ru-central1-a"
}

data "yandex_compute_image" "ubuntu_image" {
  family = "ubuntu-2204-lts"
}

data "yandex_vpc_subnet" "default_a" {
  name = "default-ru-central1-a"  # одна из дефолтных подсетей
}

resource "yandex_compute_instance" "nginx-1" {
  name = "nginx-1"

  resources {
    cores = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.id
      size = 15
    }
  }

  network_interface {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    nat = true
  }

  metadata = {
    user-data = "${file("./meta.yml")}"
  }
}

resource "yandex_compute_instance" "nginx-2" {
  name = "nginx-2"

  resources {
    cores = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.id
      size = 15
    }
  }

  network_interface {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    nat = true
  }

  metadata = {
    user-data = "${file("./meta.yml")}"
  }
}

resource "yandex_compute_instance" "reactjs-1" {
  name = "reactjs-1"

  resources {
    cores = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu_image.id
      size = 15
    }
  }

  network_interface {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    nat = true
  }

  metadata = {
    user-data = "${file("./meta.yml")}"
  }
}

resource "yandex_lb_network_load_balancer" "load-balancer" {
  name = "load-balancer"

  listener {
    name = "listener-web-servers"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.web-servers.id

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}

resource "yandex_lb_target_group" "web-servers" {
  name = "web-servers-target-group"

  target {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    address   = yandex_compute_instance.nginx-1.network_interface.0.ip_address
  }

  target {
    subnet_id = data.yandex_vpc_subnet.default_a.subnet_id
    address   = yandex_compute_instance.nginx-2.network_interface.0.ip_address
  }
}

output "subnet_id" {
    value = data.yandex_vpc_subnet.default_a.subnet_id
}

output "external_ip_address_nginx_1" {
  value = yandex_compute_instance.nginx-1.network_interface.0.nat_ip_address
}

output "external_ip_address_nginx_2" {
  value = yandex_compute_instance.nginx-2.network_interface.0.nat_ip_address
}

output "external_ip_address_reactjs" {
  value = yandex_compute_instance.reactjs-1.network_interface.0.nat_ip_address
}

output "load_balancer_ip_address" {
  value = yandex_lb_network_load_balancer.load-balancer.listener
}
